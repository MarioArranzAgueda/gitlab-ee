# frozen_string_literal: true

module Gitlab
  module CodeOwners
    FILE_NAME = 'CODEOWNERS'
    FILE_PATHS = [FILE_NAME, "docs/#{FILE_NAME}", ".gitlab/#{FILE_NAME}"].freeze

    def self.for_blob(blob)
      if blob.project.feature_available?(:code_owners)
        Loader.new(blob.project, blob.commit_id, blob.path).members
      else
        User.none
      end
    end
  end
end
